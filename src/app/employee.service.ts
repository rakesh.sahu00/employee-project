import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class EmployeeService {

  constructor(private httpclient : HttpClient) {


   }
   public getEmployee(){
    return this.httpclient.get("https://dummy.restapiexample.com/api/v1/employees");
   }
   public findEmployeeById(empId:any){
    return this.httpclient.get("https://dummy.restapiexample.com/api/v1/employee/"+ empId);
   }
   public createEmployee(empdata:any){
    return this.httpclient.post("https://dummy.restapiexample.com/api/v1/create",empdata);
   }
   public deleteEmpById(empid :any){
    return this.httpclient.delete("https://dummy.restapiexample.com/api/v1/delete/"+empid)
   }
}
