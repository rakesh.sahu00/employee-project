import { Component } from '@angular/core';
import { EmployeeService } from '../employee.service';

@Component({
  selector: 'app-employee-list',
  templateUrl: './employee-list.component.html',
  styleUrl: './employee-list.component.css'
})
export class EmployeeListComponent {
  public employees : any;
  public headers :any;
  constructor(private empservice : EmployeeService){
    this.getEmployees();
  }
  public getEmployees(){
    this.empservice.getEmployee().subscribe(
      (resp)=>{
        this.employees = resp;
        this.headers = Object.keys(this.employees.data[0]);
        this.employees = this.employees.data;
        console.log(this.employees);
        console.log(this.headers)
      }
    )

  }
  public deleteById(event:any){
    
  }

}
